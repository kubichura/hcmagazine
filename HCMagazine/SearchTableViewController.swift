//
//  SearchTableViewController.swift
//  HCMagazine
//
//  Created by Julia Edisa Kumala on 20/07/18.
//  Copyright © 2018 HC Bank BJB. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController, UISearchResultsUpdating {

	var newsArray : [News] = []
	var filteredArr = [News]()
	var images_cache = [String:UIImage]()
	var images = [String]()
	var searchController = UISearchController()
	let searchURL = "http://mobs.ayobandung.com/index.php/news_controller/searchNews"
	var spinner: UIActivityIndicatorView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		navigationController?.navigationBar.barTintColor = UIColor.init(red: 17, green: 91, blue: 128) //dark blue
		navigationController?.navigationBar.titleTextAttributes =
			[NSForegroundColorAttributeName: UIColor.white]
		
		spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
		spinner.color =  UIColor.init(red: 17, green: 91, blue: 128)
		tableView.backgroundView = spinner
		
		let drawer_button = UIBarButtonItem(image: UIImage(named: "drawer-button"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
		drawer_button.tintColor = UIColor.white
		self.navigationItem.leftBarButtonItem = drawer_button
		
		self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
		
		searchController = UISearchController(searchResultsController: nil)
		searchController.searchBar.barTintColor = UIColor.init(red: 17, green: 91, blue: 128)
		searchController.searchBar.isTranslucent = true
		searchController.searchBar.tintColor = UIColor.white
		tableView.tableHeaderView = searchController.searchBar
		searchController.searchResultsUpdater = self
		if #available(iOS 9.1, *) {
			searchController.obscuresBackgroundDuringPresentation = false
		} else {
			// Fallback on earlier versions
		}
		searchController.searchBar.placeholder = "Search"
		definesPresentationContext = true
		tableView.separatorStyle = UITableViewCellSeparatorStyle.none
	}
	

	// MARK - Connection to API
	
	func getData(_ link:String, keyword:String) {
		let url:URL = URL(string: link)!
		let session = URLSession.shared
		let params = "keyword=\(keyword)"
		
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		request.httpBody = params.data(using: String.Encoding.utf8)
		//request.setValue("charset=utf-8", forHTTPHeaderField: "Content-Type")
		
		let task = session.dataTask(with: request){ (data: Data?, response: URLResponse?, error: Error?) in
			
			guard error == nil else{
				print("error calling POST")
				print(error!)
				self.alertPop("Batal", "Gagal mengambil data")
				return
			}
			
			guard let responseData = data else{
				print("Error: did not receive data")
				self.alertPop("Batal", "Gagal menerima data")
				return
			}
			
			// parse the result as JSON, since that's what the API provides
			do {
				let json = try JSON(data: responseData)
				let state = json["status"].stringValue
				print("The state is: \(state)")
				let newsData = json["data"].arrayValue
				var i = 0
				if(state=="success"){
					while (i<newsData.count){
						let imglink = "http://mobs.ayobandung.com/images-data/naskah/\(newsData[i]["edition_id"].stringValue)/\(newsData[i]["news_thumb"].stringValue)"
						let newNews = News(newsId: newsData[i]["news_id"].intValue, newsTitle: newsData[i]["news_title"].stringValue, newsSummary:newsData[i]["news_summary"].stringValue, newsContent:newsData[i]["news_content"].stringValue, newsGimmick:newsData[i]["gimmick"].stringValue, rubricId:newsData[i]["rubric_id"].intValue, rubricTitle: newsData[i]["rubric_title"].stringValue, rubricSummary: newsData[i]["rubric_summary"].stringValue, editionId:newsData[i]["edition_id"].intValue, newsThumb: imglink, newsLiked: newsData[i]["counter"].intValue, newsHits: newsData[i] ["hits"].intValue, newsComment: newsData[i] ["comment"].intValue)
						self.images.append(imglink)
						self.newsArray.append(newNews)
						i += 1
					}
				}else{
					DispatchQueue.main.async {
						OperationQueue.main.addOperation() {
							self.alertPop("Info", "Tidak ditemukan artikel dengan kata kunci ini. Coba kembali.")
							self.searchController.searchBar.text = ""
						}
					}
				}
			} catch  {
				print("error trying to convert data to JSON")
				self.alertPop("Batal", "Gagal mengonversi data")
				return
			}
		}
		task.resume()
		
	}
	
	// MARK : Search Results Updating
	
	func updateSearchResults(for searchController: UISearchController) {
		filterContentForSearchText(searchController.searchBar.text!)
	}

	// MARK: - Private instance methods
	
	func searchBarIsEmpty() -> Bool {
		// Returns true if the text is empty or nil
		return searchController.searchBar.text?.isEmpty ?? true
	}
	
	func filterContentForSearchText(_ searchText: String) {
		self.spinner.startAnimating()
		newsArray.removeAll()
		images_cache.removeAll()
		images.removeAll()
		tableView.separatorStyle = UITableViewCellSeparatorStyle.none
		getData(searchURL, keyword: searchText.lowercased())
		DispatchQueue.main.async {
			Thread.sleep(forTimeInterval: 1)
			OperationQueue.main.addOperation() {
				self.spinner.stopAnimating()
				self.tableView.reloadData()
			}
		}
	}
	
	// Asynchronously download image and set to table view
	func load_image(_ link:String, imageview:UIImageView)
	{
		let url:URL = URL(string: link)!
		let session = URLSession.shared
		
		let request = NSMutableURLRequest(url: url)
		request.timeoutInterval = 10
		
		let task = session.dataTask(with: request as URLRequest, completionHandler: {(
			data, response, error) in
			
			guard let _:Data = data, let _:URLResponse = response, error == nil else {
				return
			}
			var image = UIImage(data: data!)
			
			if (image != nil){
				
				func set_image(){
					self.images_cache[link] = image
					imageview.image = image
				}
				DispatchQueue.main.async(execute: set_image)
			}
		})
		task.resume()
	}
	
    // MARK: - Table view data source
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 132.0
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		if self.newsArray.isEmpty == true{
			let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
			noDataLabel.text          = "Type in search box to begin searching"
			noDataLabel.textColor     = UIColor.darkGray
			noDataLabel.textAlignment = .center
			tableView.backgroundView  = noDataLabel
			tableView.separatorStyle  = .none
			
			return 0
		}else{
			tableView.backgroundView = nil
			return 1
		}
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return newsArray.count
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RubricCell
		let getNews = newsArray[indexPath.row]
		
		cell.articleTitle.text = getNews.newsTitle
		cell.summary.text = getNews.newsSummary
		cell.rubricTitle.text = getNews.rubricTitle
		cell.counter.text = "\(getNews.newsLiked )"
		cell.readCounter.text = "\(getNews.newsHits )"
		cell.commentCounter.text = "\(getNews.newsComment )"
		
		if (images_cache[images[indexPath.row]] != nil){
			cell.articleThumb.image = images_cache[images[indexPath.row]]
		}else{
			load_image(images[indexPath.row], imageview:cell.articleThumb)
		}
		
		//Change background color
		let bgColorView = UIView()
		bgColorView.backgroundColor = UIColor.init(red: 234, green: 192, blue: 68)
		cell.selectedBackgroundView = bgColorView
		
		return cell
	}
	
	// Cell is selected
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if let cell = tableView.cellForRow(at: indexPath){
			let getNews = newsArray[indexPath.row]
			performSegue(withIdentifier: "OpenArticleSegue", sender: cell)
		}else {
			// Error indexPath is not on screen: this should never happen.
		}
	}
	
	// Add animation
	override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		/* only fade in
		cell.alpha = 0
		
		UIView.animate(withDuration: 1.0){
		cell.alpha = 1.0
		} */
		
		cell.alpha = 0
		let transform = CATransform3DTranslate(CATransform3DIdentity,-250,20,0)
		cell.layer.transform = transform
		
		UIView.animate(withDuration: 1.0){
			cell.alpha = 1.0
			cell.layer.transform = CATransform3DIdentity
		}
	}
	
	// Set up data to be passed
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "OpenArticleSegue" {
			if let indexPath = self.tableView.indexPath(for: sender as! RubricCell) {
				let navVC = segue.destination as? UINavigationController
				let articleVC = navVC?.viewControllers.first as! ArticleViewController
				articleVC.content = newsArray[indexPath.row].newsContent
				articleVC.gimmick = newsArray[indexPath.row].newsGimmick
				articleVC.rubTitle = newsArray[indexPath.row].rubricTitle
				articleVC.id = newsArray[indexPath.row].newsId
				articleVC.rubId = newsArray[indexPath.row].rubricId
				articleVC.totalLikes = newsArray[indexPath.row].newsLiked
				articleVC.totalHits = newsArray[indexPath.row].newsHits
				articleVC.totalComments = newsArray[indexPath.row].newsComment
				articleVC.search = true
			}
			
		}
	}

	// MARK: - Alert
	
	func alertPop(_ titles: String, _ msg: String) {
		let Alert = UIAlertController(title: titles, message: msg, preferredStyle: UIAlertControllerStyle.alert)
		
		Alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
			print("Dismiss for '\(titles)'")
		}))
		self.present(Alert, animated: true, completion: nil)
	}
}
